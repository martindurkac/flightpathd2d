#pragma once

#include "IPolygonProvider.h"
#include "DrawDoc.h"
#include "LerpVector.h"

class SplinePath : public IPolygonPointProvider
{
private:
    class Bezier3 // Cubic Bezier
    {
    private:
        Vect2f _pos;
        Vect2f _dir;
        float  _len;
        bool   _hasPrev;
        bool   _hasNext;
        LerpVector _len2t;
        const Bezier3* Prev() const { return _hasPrev ? this - 1 : nullptr; }
        float GetVectToThis(Vect2f& v) const;
        float GetVectToNext(Vect2f& v) const;
        void SetDirection(float turnRadius, float len, const Vect2f& v, float vLen);
        void CalculateLength();
        float RefineLength(
            float len,
            const Vect2f& p0,
            const Vect2f& p1,
            float t0,
            float t1,
            float eps,
            std::map<float, Vect2f>& points);
    public:
        Bezier3(const Vect2f& point);
        void SetPrevious(Bezier3* ptr);
        void SetDirection(float turnRadius, const float* heading = nullptr);
        float GetLength() const { return _len; }
        Vect2f GetPoint(float t) const;
        Vect2f GetPointByLength(float len) { return GetPoint(_len2t(len)); }
    };

    DocColor m_color;
    DocPath m_route;

    float m_startHeading;
    float m_startSpeed;
    float m_startBank;
    float m_simTimeStep;
    float m_distanceStep;

    std::vector<Bezier3> m_segments;
    float m_segmentDistance;
    size_t m_segmentIdx;

    D2D1_POINT_2F m_pointBuffer;

public:
    SplinePath(const DocModel& model);
    virtual ~SplinePath();

    virtual PolygonType GetType();
    virtual UINT32 GetColor();
    virtual const D2D1_POINT_2F* GetFirstPoint();
    virtual const D2D1_POINT_2F* GetNextPoint();
};

inline void SplinePath::Bezier3::SetPrevious(Bezier3* ptr)
{
    if (ptr == this - 1)
    {
        _hasPrev = true;
        ptr->_hasNext = true;
    }
}

inline float SplinePath::Bezier3::GetVectToThis(Vect2f& v) const
{
    if (_hasPrev) 
    { 
        v = _pos - (this - 1)->_pos; 
        return v.length();
    }
    return .0f;
}

inline float SplinePath::Bezier3::GetVectToNext(Vect2f& v) const
{
    if (_hasNext)
    {
        v = (this + 1)->_pos - _pos;
        return v.length();
    }
    return .0f;
}
