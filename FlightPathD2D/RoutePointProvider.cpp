#include "RoutePointProvider.h"

RoutePointProvider::RoutePointProvider(const DocModel& model)
    : m_pointIdx(0)
    , m_color(0x800000)
{
    if (model._path.size() > 0)
    {
        m_points.reserve(model._path.size());
        for (auto point : model._path)
        {
            m_points.push_back(point);
        }
    }
}

RoutePointProvider::~RoutePointProvider()
{}

IPolygonPointProvider::PolygonType RoutePointProvider::GetType()
{
    return RoutingPoints;
}

UINT32 RoutePointProvider::GetColor()
{
    return m_color;
}

const D2D1_POINT_2F* RoutePointProvider::GetFirstPoint()
{
    m_pointIdx = 0;
    return GetNextPoint();
}

const D2D1_POINT_2F* RoutePointProvider::GetNextPoint()
{
    if (m_pointIdx < m_points.size())
        return m_points.data() + m_pointIdx++;
    return nullptr;
}

