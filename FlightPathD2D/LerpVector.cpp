#include "LerpVector.h"

float LerpVector::operator() (float x)
{
    if (_samples.empty())
        return .0f;

    auto bound = _samples.lower_bound(x);
    if (bound == _samples.end())
    {
        // x is greater than max-x (last sample)
        return (--bound)->second;
    }
    else if (bound == _samples.begin())
    {
        // x is less than min-x (1st sample)
        return bound->second;
    }
    else
    {
        // x is between 2 samples
        auto x2 = bound->first;
        auto y2 = bound->second;
        --bound;
        auto x1 = bound->first;
        auto y1 = bound->second;

        float dx = x2 - x1;

        if (x < x1 || dx < 1e-5f)
            return y1;

        return y1 + (x - x1) * (y2 - y1) / dx;
    }
}
