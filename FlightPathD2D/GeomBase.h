#pragma once

#include <d2d1.h>
#include <d2d1helper.h>
#include <math.h>

#define DEG_TO_RAD(deg)     ((deg)*(PI_F/180.f))
#define RAD_TO_DEG(rad)     ((rad)*(180.f/PI_F))
#define ANGLE_TO_AZM(angle) (PI_2_F - (angle))
#define AZM_TO_ANGLE(azim)  (PI_2_F - (azim))
#define KMPH_TO_MPS(v)      ((v)/3.6f)
#define MPS_TO_KMPH(v)      ((v)*3.6f)
#define KM_TO_M(v)          ((v)*1e3f)
#define M_TO_KM(v)          ((v)*1e-3f)

const float Epsilon = 1e-4f;
const float PI_F = 3.14159265358979f;
const float PI_2_F = .5f * PI_F;
const float gAccel = 9.81f;

inline float GetTurnRadius(float v, float b) { return v * v / gAccel / tanf(b); }

class Line;
class Circle;

class Vect2f
{
private:
    float _x;
    float _y;

public:
    enum CoordsType { Cartesian, Polar, Azimuthal };
    enum DirectionType { None, CCW, CW };
    enum SideType { Rear, Left, Right, Front,  };

    Vect2f() : _x(.0f), _y(.0f) {}
    Vect2f(float x, float y) : _x(x), _y(y) {}
    Vect2f(const Vect2f& v) : _x(v._x), _y(v._y) {}
    Vect2f(float a, float b, CoordsType coords);

    Vect2f& operator=(const Vect2f& v) { _x = v._x, _y = v._y; return *this; }

    SideType side(const Vect2f& pos, const Vect2f& dir, float eps = .0f) const;
    float angle(DirectionType dir, const Vect2f& a, const Vect2f& b) const;
    Vect2f normal(DirectionType direction, float lenghtFactor = -1.) const;

    float angle() const { return atan2f(_y, _x); }
    float length() const { return hypotf(_x, _y); }
    float angleTo(const Vect2f& dest) const { return atan2f(dest._y - _y, dest._x - _x); }
    float dot(const Vect2f& v) const { return _x * v._x + _y * v._y; }
    bool closerThan(float distance, const Vect2f& pos) const 
    {
        Vect2f diff = (*this) - pos;
        return diff.dot(diff) < distance * distance;
    }

    Vect2f reverse() const { return Vect2f(-_x, -_y); }
    Vect2f operator+(const Vect2f& v) const { return Vect2f(_x + v._x, _y + v._y); }
    Vect2f operator-(const Vect2f& v) const { return Vect2f(_x - v._x, _y - v._y); }
    Vect2f operator*(const float a) const { return Vect2f(_x * a, _y * a); }
    Vect2f operator/(const float a) const { return Vect2f(_x / a, _y / a); }
    void operator+=(const Vect2f& v) { _x += v._x; _y += v._y; }
    void operator-=(const Vect2f& v) { _x -= v._x; _y -= v._y; }

    operator D2D1_POINT_2F () const { return D2D1::Point2F(_x, _y); }

    friend Line;
    friend Circle;
};

class Circle
{
private:
    Vect2f _center;
    float _radius;
public:
    Circle(const Vect2f& c, float r)
        : _center(c)
        , _radius(r)
    {}
    friend Line;
};

class Line
{
private:
    Vect2f _origin;
    Vect2f _direction;
public:
    Line(const Vect2f& o, const Vect2f& d)
        : _origin(o)
        , _direction(d) 
    {}
    int calcIntersections(float *t, const Circle& circle);
    Vect2f calcPoint(float t) { return _origin + _direction * t; }
};
