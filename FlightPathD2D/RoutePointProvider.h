#pragma once

#include "IPolygonProvider.h"
#include "DrawDoc.h"

class RoutePointProvider : public IPolygonPointProvider
{
public:
    RoutePointProvider(const DocModel& model);
    virtual ~RoutePointProvider();

    virtual PolygonType GetType();
    virtual UINT32 GetColor();
    virtual const D2D1_POINT_2F* GetFirstPoint();
    virtual const D2D1_POINT_2F* GetNextPoint();

private:
    size_t m_pointIdx;
    UINT32 m_color;
    std::vector<D2D1_POINT_2F> m_points;
};

