#pragma once

#include "IPolygonProvider.h"
#include "DrawDoc.h"
#include "TurnCalculator.h"

class PathSimulator : public IPolygonPointProvider
{
private:
    DocColor m_color;
    DocPath m_route;

    float m_startHeading;
    float m_startSpeed;
    float m_startBank;
    float m_simTimeStep;

    float m_simTime;
    float m_turnRadius;
    float m_distanceStep;
    size_t m_routeIndex;

    TurnCalculator m_turnCalc;

    D2D1_POINT_2F m_output;

    const D2D1_POINT_2F* Reset();
    const D2D1_POINT_2F* Simulate();

public:
    PathSimulator(const DocModel& model);
    virtual ~PathSimulator();

    virtual PolygonType GetType();
    virtual UINT32 GetColor();
    virtual const D2D1_POINT_2F* GetFirstPoint();
    virtual const D2D1_POINT_2F* GetNextPoint();
};