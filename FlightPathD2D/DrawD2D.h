#pragma once

#include <d2d1.h>

#include "IPolygonProvider.h"
#include "DrawDoc.h"

class DrawD2D
{
private:
    ID2D1HwndRenderTarget *m_pTargetD2D;

    D2D1_SIZE_F m_targetSize;
    D2D1_RECT_F m_viewRect;
    float m_pixelSize;
    float m_viewWidth;
    D2D1::Matrix3x2F m_viewTransformation;

    ID2D1SolidColorBrush* m_pSolidBrush;

    IPolygonProvider* m_pProvider;

private:
    HRESULT CreateResources(HWND hwnd);
    void ReleaseResources();
    void SetTransformation();

    void DrawGrid();
    void DrawPolygons();
    D2D1_RECT_F MakeRect(const D2D1_POINT_2F& center, float grow);

public:
    DrawD2D();
    ~DrawD2D();

    void SetDataProvider(IPolygonProvider* pProvider) {
        m_pProvider = pProvider;
    }

    void OnDraw(HWND hwnd);
    void OnResize(UINT width, UINT height);
};

inline D2D1_RECT_F DrawD2D::MakeRect(const D2D1_POINT_2F& center, float grow)
{
    return D2D1::RectF(
        center.x - grow, // left
        center.y + grow, // top
        center.x + grow, // right
        center.y - grow);// bottom
}

template<class Interface>
inline void SafeRelease(Interface **ppi)
{
    if (*ppi)
    {
        (*ppi)->Release();
        (*ppi) = NULL;
    }
}
