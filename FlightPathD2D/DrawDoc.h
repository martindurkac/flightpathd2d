#pragma once

#include <string>
#include <vector>
#include <memory>
#include <map>

#include <nlohmann/json.hpp>

#include "IPolygonProvider.h"
#include "GeomBase.h"

typedef std::vector<Vect2f> DocPath;
typedef UINT32 DocColor;

struct DocModel
{
    float _dt;
    float _heading;
    float _speed;
    float _bank;
    DocPath _path;
    DocColor _color;
};

class DrawDoc : public IPolygonProvider
{
private:
    size_t m_providerIdx = 0;
    float m_viewWidth = .0f;
    std::vector<float> m_gridSteps;
    std::map<std::string, DocColor> m_colors;
    std::map<std::string, DocPath> m_paths;
    std::map<std::string, DocModel> m_models;
    std::vector<std::unique_ptr<IPolygonPointProvider>> m_drawDataProviders;
    std::string m_fileName;

    void LoadViewPort(const nlohmann::json& node);
    void LoadDrawList(const nlohmann::json& node);
    void LoadColors(const nlohmann::json& node);
    void LoadPaths(const nlohmann::json& node);
    void LoadModels(const nlohmann::json& node);
    void LoadPath(DocPath& path, const nlohmann::json& node);

public:
    DrawDoc() {};
    ~DrawDoc() {};

    void SetFile(const char* fname) { m_fileName = (fname ? fname : ""); }
    bool Reload();

    virtual float GetViewWidth();
    virtual size_t GetGridSteps(float* steps, size_t size);
    virtual IPolygonPointProvider* GetFirstPolygon();
    virtual IPolygonPointProvider* GetNextPolygon();
};