#pragma once

#include "DrawDoc.h"
#include "DrawD2D.h"

class DemoApp
{
public:
    DemoApp();
    ~DemoApp();

    HRESULT Initialize(LPCSTR cmdLine);
    void RunMessageLoop();

private:
    void OnRender() { m_DrawD2D.OnDraw(m_hwnd); }
    void OnResize(UINT width, UINT height) { m_DrawD2D.OnResize(width, height); }
    void OnCharMsg(WCHAR c);
    void ShowSimStats();
    void GetSimStats(
        DWORD& timeSpan,
        DWORD& pointCount,
        IPolygonPointProvider::PolygonType pt);

    // The windows procedure.
    static LRESULT CALLBACK WndProc(
        HWND hWnd,
        UINT message,
        WPARAM wParam,
        LPARAM lParam);

private:
    HWND m_hwnd;
    DrawDoc m_DrawDoc;
    DrawD2D m_DrawD2D;
};
