#pragma once

#include <d2d1.h>

class IPolygonPointProvider
{
public:
    enum PolygonType { RoutingPoints, SimulatedTrack, BezierTrack };

    virtual PolygonType GetType() = 0;
    virtual UINT32 GetColor() = 0;
    virtual const D2D1_POINT_2F* GetFirstPoint() = 0;
    virtual const D2D1_POINT_2F* GetNextPoint() = 0;
};

class IPolygonProvider
{
public:
    virtual float GetViewWidth() = 0;
    virtual size_t GetGridSteps(float* steps, size_t size) = 0;
    virtual IPolygonPointProvider* GetFirstPolygon() = 0;
    virtual IPolygonPointProvider* GetNextPolygon() = 0;
};
