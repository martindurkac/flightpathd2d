#include "GeomBase.h"

Vect2f::Vect2f(float a, float b, CoordsType coords)
{
    switch (coords)
    {
    case Polar:
        _x = b * cosf(a);
        _y = b * sinf(a);
        break;

    case Azimuthal:
        _x = b * cosf(AZM_TO_ANGLE(a));
        _y = b * sinf(AZM_TO_ANGLE(a));
        break;

    case Cartesian:
    default:
        _x = a;
        _y = b;
        break;
    }
}

Vect2f Vect2f::normal(DirectionType direction, float lenghtFactor /*= .0f */) const
{
    if (lenghtFactor > .0f)
    {
        switch (direction)
        {
        case CCW: return { -_y * lenghtFactor, _x * lenghtFactor };
        case CW:  return { _y * lenghtFactor, -_x * lenghtFactor };
        default: break;
        }
    }
    else
    {
        switch (direction)
        {
        case CCW: return { -_y, _x };
        case CW:  return { _y, -_x };
        default: break;
        }
    }
    return { .0f, .0f };
}

Vect2f::SideType Vect2f::side(const Vect2f& pos, const Vect2f& dir, float eps /*= .0f*/) const
{
    //       F                 x - this point
    //       ^                 ^ - dir - direction vector
    //   L   |   R             F L R Rr - evaluated positions
    //       x                     L/R  -> Left/Right
    //       Rr                    F/Rr -> Front/Rear
    
    Vect2f toPos(pos - *this);
    Vect2f dirCcwN(dir.normal(CCW));
    float dot = toPos.dot(dirCcwN);

    if (!eps)
    {
        return
            (dot == .0f) ? (toPos.dot(dir) > .0 ? Front : Rear) :
            (dot > .0f) ? Left : Right;
    }
    else
    {
        return
            (fabsf(dot) < eps) ? (toPos.dot(dir) > .0 ? Front : Rear) :
            (dot > .0f) ? Left : Right;
    }
}

float Vect2f::angle(DirectionType direction, const Vect2f& a, const Vect2f& b) const
{
    //
    //   +b    +a
    //    \   /         CCW angle is positive value from <0, 2PI)
    //     \ /          CW  angle is negative value from (-2PI, 0>
    //      +this       angle apex is *this position 
    //                  apex side on picture is Left
    //                  angle on pict is approx CCW: 30 deg, CW: -330 deg
    //
    Vect2f toA(a - *this);
    Vect2f toB(b - *this);
    float angle = acosf(toA.dot(toB) / toA.length() / toB.length());
    SideType apexSide = a.side(*this, b - a);

    switch (direction)
    {
    case CCW: return apexSide == Right ? 2.0f * PI_F - angle : angle;
    case CW:  return apexSide == Left  ? angle - 2.0f * PI_F : -angle;
    default:  return angle;
    }
}

int Line::calcIntersections(float *t, const Circle& circle)
{
    Vect2f diff = _origin - circle._center;
    float a = _direction.dot(_direction);
    float b = _direction.dot(diff) * 2.f;
    float c = diff.dot(diff) - circle._radius * circle._radius;
    float d = b * b - 4 * a * c;

    if (fabs(d) < Epsilon)
    {
        t[0] = -b / (2 * a);
        return 1;
    }
    else if (d > 0)
    {
        float sqrtd = sqrtf(d);
        t[0] = (-b - sqrtd) / (2 * a);
        t[1] = (-b + sqrtd) / (2 * a);
        return 2;
    }
    return 0;
}
