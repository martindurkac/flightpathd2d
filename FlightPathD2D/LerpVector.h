#pragma once

#include <map>

class LerpVector
{
private:
    std::map<float, float> _samples;

public:
    void Reset() { _samples.clear(); }
    void AddSample(float x, float y) { _samples[x] = y; }
    float operator() (float x);
};
