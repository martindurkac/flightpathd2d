#include "DrawD2D.h"

DrawD2D::DrawD2D()
    : m_pTargetD2D(nullptr)
    , m_pSolidBrush(nullptr)
    , m_pProvider(nullptr)
    , m_viewWidth(-1.f)
{
}

DrawD2D::~DrawD2D()
{
    ReleaseResources();
}

void DrawD2D::ReleaseResources()
{
    SafeRelease(&m_pTargetD2D);
    SafeRelease(&m_pSolidBrush);
}

HRESULT DrawD2D::CreateResources(HWND hwnd)
{
    HRESULT hr = S_OK;

    if (!m_pTargetD2D)
    {
        ID2D1Factory* factory = nullptr;
        
        // Create a Direct2D factory.
        hr = D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED, &factory);

        if (SUCCEEDED(hr))
        {
            RECT rc;
            GetClientRect(hwnd, &rc);

            D2D1_SIZE_U size = D2D1::SizeU(
                rc.right - rc.left,
                rc.bottom - rc.top
            );

            // Create a Direct2D render target.
            hr = factory->CreateHwndRenderTarget(
                D2D1::RenderTargetProperties(),
                D2D1::HwndRenderTargetProperties(hwnd, size),
                &m_pTargetD2D);

            if (SUCCEEDED(hr))
            {
                // Create a solid brush.
                hr = m_pTargetD2D->CreateSolidColorBrush(
                    D2D1::ColorF(D2D1::ColorF::CornflowerBlue),
                    &m_pSolidBrush);
            }

            SafeRelease(&factory);
        }
    }
    return hr;
}

void DrawD2D::SetTransformation()
{
    // Update transformation when view width is changed
    float viewWidth = m_pProvider->GetViewWidth();
    if (viewWidth != m_viewWidth)
    {
        m_viewWidth = viewWidth;
        m_targetSize = m_pTargetD2D->GetSize();
        float scale = m_targetSize.width / viewWidth;
        auto origin = D2D1::Point2F(
            .5f * m_targetSize.width,
            .5f * m_targetSize.height);

        m_pixelSize = 1.f / scale;
        float viewHeight = m_targetSize.height * m_pixelSize;

        m_viewRect = D2D1::RectF(
            -.5f * viewWidth, .5f * viewHeight,
            .5f * viewWidth, -.5f * viewHeight);

        m_viewTransformation =
            D2D1::Matrix3x2F::Translation(origin.x, origin.y) *
            D2D1::Matrix3x2F::Scale(scale, -scale, origin);

        m_pTargetD2D->SetTransform(m_viewTransformation);
    }
}

void DrawD2D::OnResize(UINT width, UINT height)
{
    if (m_pTargetD2D)
    {
        // Note: This method can fail, but it's okay to ignore the
        // error here, because the error will be returned again
        // the next time EndDraw is called.
        m_pTargetD2D->Resize(D2D1::SizeU(width, height));

        D2D1_SIZE_F prevSize = m_targetSize;
        m_targetSize = m_pTargetD2D->GetSize();
        m_viewRect.right += m_pixelSize * (m_targetSize.width - prevSize.width);
        m_viewRect.bottom -= m_pixelSize * (m_targetSize.height - prevSize.height);
    }
}

void DrawD2D::OnDraw(HWND hwnd)
{
    HRESULT hr = CreateResources(hwnd);

    if (SUCCEEDED(hr))
    {
        SetTransformation();

        m_pTargetD2D->BeginDraw();
        m_pTargetD2D->Clear(D2D1::ColorF(D2D1::ColorF::White));

        DrawGrid();
        DrawPolygons();

        hr = m_pTargetD2D->EndDraw();
    }

    if (hr == D2DERR_RECREATE_TARGET)
    {
        ReleaseResources();
    }
}

void DrawD2D::DrawGrid()
{
    UINT32 gridColors[3]{ 
        0xf0f0ff, // small grid
        0xd0d0e0, // large grid
        0x404040  // center cross
    };
    float gridSteps[2];
    size_t stepCount = m_pProvider->GetGridSteps(gridSteps, 2);
    for (size_t i = 0; i < stepCount; ++i)
    {
        float step = gridSteps[i];
        D2D1_POINT_2F a, b;
        D2D1_POINT_2F gridRightTop = D2D1::Point2F(
            m_viewRect.right - fmodf(m_viewRect.right, step),
            m_viewRect.top - fmodf(m_viewRect.top, step));

        m_pSolidBrush->SetColor(D2D1::ColorF(gridColors[i]));

        // draw vertical grid lines
        a.y = m_viewRect.bottom;
        b.y = m_viewRect.top;
        for (float x = gridRightTop.x; x > m_viewRect.left; x -= step)
        {
            a.x = b.x = x;
            m_pTargetD2D->DrawLine(a, b, m_pSolidBrush, m_pixelSize);
        }

        // draw horizontal grid lines
        a.x = m_viewRect.left;
        b.x = m_viewRect.right;
        for (float y = gridRightTop.y; y > m_viewRect.bottom; y -= step)
        {
            a.y = b.y = y;
            m_pTargetD2D->DrawLine(a, b, m_pSolidBrush, m_pixelSize);
        }
    }

    // draw 40 pixel cross in center point [0,0]
    float halfSz = 20.f * m_pixelSize;
    D2D1_POINT_2F c, d, e, f;
    c.x = d.x = e.y = f.y =.0f;
    c.y = e.x = halfSz;
    d.y = f.x = -halfSz;
    m_pSolidBrush->SetColor(D2D1::ColorF(gridColors[2]));
    m_pTargetD2D->DrawLine(c, d, m_pSolidBrush, m_pixelSize);
    m_pTargetD2D->DrawLine(e, f, m_pSolidBrush, m_pixelSize);
}

void DrawD2D::DrawPolygons()
{
    for (auto pPolygon = m_pProvider->GetFirstPolygon();
        pPolygon != nullptr;
        pPolygon = m_pProvider->GetNextPolygon())
    {
        m_pSolidBrush->SetColor(D2D1::ColorF(
            pPolygon->GetColor()));

        auto polygonType = pPolygon->GetType();
        float growSize = m_pixelSize * (
            (polygonType == IPolygonPointProvider::RoutingPoints) ? 2.5f : 1.f);

        for (auto pPoint = pPolygon->GetFirstPoint();
            pPoint != nullptr;
            pPoint = pPolygon->GetNextPoint())
        {
            auto r = MakeRect(*pPoint, growSize);
            m_pTargetD2D->DrawRectangle(&r, m_pSolidBrush, m_pixelSize);
        }
    }
}
