#include <fstream>

#include "RoutePointProvider.h"
#include "PathSimulator.h"
#include "SplinePath.h"
#include "DrawDoc.h"

IPolygonPointProvider* DrawDoc::GetFirstPolygon()
{
    m_providerIdx = 0;
    return GetNextPolygon();
}

IPolygonPointProvider* DrawDoc::GetNextPolygon()
{
    if (m_providerIdx < m_drawDataProviders.size())
        return m_drawDataProviders[m_providerIdx++].get();
    return nullptr;
}

float DrawDoc::GetViewWidth()
{
    return m_viewWidth;
}

size_t DrawDoc::GetGridSteps(float* steps, size_t size) 
{
    if (!steps)
    {
        return m_gridSteps.size();
    }
    else
    {
        size_t n = min(size, m_gridSteps.size());
        for (size_t i = 0; i < n; ++i)
            steps[i] = m_gridSteps[i];
        return n;
    }
}

bool DrawDoc::Reload()
{
    bool result = false;

    std::ifstream ifs(m_fileName, std::ios_base::in);
    if (ifs.is_open())
    {
        // parse file - no callback, no exception
        nlohmann::json root = nlohmann::json::parse(ifs, nullptr, false);

        if (root.is_object())
        {
            LoadViewPort(root["viewport"]);
            LoadColors(root["colors"]);
            LoadPaths(root["paths"]);
            LoadModels(root["models"]);
            LoadDrawList(root["drawlist"]);
            result = true;
        }
    }

    return result;
}

void DrawDoc::LoadDrawList(const nlohmann::json& node)
{
    m_drawDataProviders.clear();

    for (auto it = node.begin(); it != node.end(); ++it)
    {
        if (it->is_string())
        {
            auto found = m_models.find(*it);
            if (found != m_models.end())
            {
                m_drawDataProviders.push_back(
                    std::unique_ptr<IPolygonPointProvider>(
                        new RoutePointProvider(found->second)));

                m_drawDataProviders.push_back(
                    std::unique_ptr<IPolygonPointProvider>(
                        new PathSimulator(found->second)));

                m_drawDataProviders.push_back(
                    std::unique_ptr<IPolygonPointProvider>(
                        new SplinePath(found->second)));
            }
        }
    }
}

void DrawDoc::LoadViewPort(const nlohmann::json& node)
{
    if (node.is_object())
    {
        node["width"].get_to(m_viewWidth); 
        node["grid"].get_to(m_gridSteps);

        m_viewWidth = KM_TO_M(m_viewWidth);
        for (auto it = m_gridSteps.begin(); it != m_gridSteps.end(); ++it)
        {
            *it = KM_TO_M(*it);
        }
    }
}

void DrawDoc::LoadColors(const nlohmann::json& node)
{
    if (node.is_object())
    {
        m_colors.clear();
        for (auto item : node.items())
        {
            if (item.value().is_string())
            {
                m_colors[item.key()] = std::stoul(
                    item.value().get<std::string>(),
                    nullptr,
                    16);
            }
        }
    }
}

void DrawDoc::LoadPaths(const nlohmann::json& node)
{
    if (node.is_object())
    {
        m_paths.clear();
        for (auto item : node.items())
        {
            LoadPath(m_paths[item.key()], item.value());
        }
    }
}

void DrawDoc::LoadPath(DocPath& path, const nlohmann::json& node)
{
    Vect2f currentPos{.0, .0};
    std::vector<float> coords;
        
    for (auto it = node.begin(); it != node.end(); ++it)
    {
        if (!it->is_object())
            continue;
        
        for (auto item : it->items())
        {
            item.value().get_to(coords);
            switch (*(item.key().c_str()))
            {
            case '.':
                currentPos =
                {
                    KM_TO_M(coords[0]),
                    KM_TO_M(coords[1])
                };
                path.push_back(currentPos);
                break;
            case '+':
                currentPos +=
                {
                    KM_TO_M(coords[0]),
                    KM_TO_M(coords[1])
                };
                path.push_back(currentPos);
                break;
            case '<':
                currentPos +=
                {
                    DEG_TO_RAD(coords[0]),
                    KM_TO_M(coords[1]),
                    Vect2f::Azimuthal
                };
                path.push_back(currentPos);
                break;
            }
            break;
        }
    }
}

void DrawDoc::LoadModels(const nlohmann::json& node)
{
    if (node.is_object())
    {
        m_models.clear();
        for (auto item : node.items())
        {
            const auto& value = item.value();
            auto& m = m_models[item.key()];
            value["dt"].get_to(m._dt);
            value["heading"].get_to(m._heading);
            value["speed"].get_to(m._speed);
            value["bank"].get_to(m._bank);
            m._heading = AZM_TO_ANGLE(DEG_TO_RAD(m._heading));
            m._bank = DEG_TO_RAD(m._bank);
            m._speed = KMPH_TO_MPS(m._speed);

            auto itColor = m_colors.find(value["color"].get<std::string>());
            m._color = (itColor != m_colors.end() ? itColor->second : 0);
            
            auto itPath = m_paths.find(value["path"].get<std::string>());
            if (itPath != m_paths.end())
                m._path = itPath->second;
        }
    }
}

