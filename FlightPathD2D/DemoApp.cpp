#include "DemoApp.h"
#include <sstream>

#ifndef HINST_THISCOMPONENT
EXTERN_C IMAGE_DOS_HEADER __ImageBase;
#define HINST_THISCOMPONENT ((HINSTANCE)&__ImageBase)
#endif

const char* MSG_PARSING_ERROR = "Failed to parse input file";
const char* MSG_MISSING_FILE = "\tMissing or invalid input file\n\n"
                               "Usage: FlightPathD2D.exe <draw_doc>.json";

DemoApp::DemoApp() : m_hwnd(NULL)
{
}

DemoApp::~DemoApp()
{
}

void DemoApp::RunMessageLoop()
{
    MSG msg;

    while (GetMessage(&msg, NULL, 0, 0))
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }
}

HRESULT DemoApp::Initialize(LPCSTR cmdLine)
{
    HRESULT hr = S_OK;

    // Register the window class.
    WNDCLASSEX wcex = { sizeof(WNDCLASSEX) };
    wcex.style = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc = DemoApp::WndProc;
    wcex.cbClsExtra = 0;
    wcex.cbWndExtra = sizeof(LONG_PTR);
    wcex.hInstance = HINST_THISCOMPONENT;
    wcex.hbrBackground = NULL;
    wcex.lpszMenuName = NULL;
    wcex.hCursor = LoadCursor(NULL, IDI_APPLICATION);
    wcex.lpszClassName = L"FlightPathD2D";

    RegisterClassEx(&wcex);

    // Create the window.
    m_hwnd = CreateWindow(
        L"FlightPathD2D",
        L"Flight Path Direct2D Demo",
        WS_OVERLAPPEDWINDOW,
        CW_USEDEFAULT,
        CW_USEDEFAULT,
        1000, // px width
        800, // px height
        NULL,
        NULL,
        HINST_THISCOMPONENT,
        this);

    hr = m_hwnd ? S_OK : E_FAIL;

    if (SUCCEEDED(hr))
    {
        m_DrawDoc.SetFile(cmdLine);
        if (!m_DrawDoc.Reload())
        {
            MessageBoxA(m_hwnd, MSG_MISSING_FILE, nullptr, MB_OK);
            exit(1);
        }

        m_DrawD2D.SetDataProvider(&m_DrawDoc);

        ShowWindow(m_hwnd, SW_SHOWNORMAL);
        UpdateWindow(m_hwnd);
    }

    return hr;
}

LRESULT CALLBACK DemoApp::WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    LRESULT result = 0;

    if (message == WM_CREATE)
    {
        LPCREATESTRUCT pcs = (LPCREATESTRUCT)lParam;
        DemoApp *pDemoApp = (DemoApp *)pcs->lpCreateParams;

        ::SetWindowLongPtrW(hwnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(pDemoApp));

        result = 1;
    }
    else
    {
        DemoApp *pDemoApp = reinterpret_cast<DemoApp *>(static_cast<LONG_PTR>(
            ::GetWindowLongPtrW(hwnd, GWLP_USERDATA)));

        bool wasHandled = false;
        if (pDemoApp)
        {
            wasHandled = true;
            switch (message)
            {
            case WM_SIZE:
                pDemoApp->OnResize(
                    LOWORD(lParam),  // width 
                    HIWORD(lParam)); // height
                break;

            case WM_DISPLAYCHANGE:
                InvalidateRect(hwnd, NULL, FALSE);
                break;

            case WM_PAINT:
                pDemoApp->OnRender();
                ValidateRect(hwnd, NULL);
                break;

            case WM_CHAR:
                pDemoApp->OnCharMsg((WCHAR)wParam);
                break;

            case WM_DESTROY:
                PostQuitMessage(0);
                result = 1;
                break;

            default:
                wasHandled = false;
                break;
            }
        }

        if (!wasHandled)
        {
            result = DefWindowProc(hwnd, message, wParam, lParam);
        }
    }

    return result;
}

void DemoApp::OnCharMsg(WCHAR c)
{
    switch (c)
    {
    case ' ':
        if (m_DrawDoc.Reload())
            InvalidateRect(m_hwnd, NULL, TRUE);
        else
            MessageBoxA(m_hwnd, MSG_PARSING_ERROR, nullptr, MB_OK);
        break;
    case 'z':
    case 'Z':
        ShowSimStats();
        break;
    }
}

void DemoApp::GetSimStats(
    DWORD& timeSpan,
    DWORD& pointCount,
    IPolygonPointProvider::PolygonType pt)
{
    DWORD startTime = GetTickCount();
    pointCount = 0;

    for (auto pPolygon = m_DrawDoc.GetFirstPolygon();
        pPolygon != nullptr;
        pPolygon = m_DrawDoc.GetNextPolygon())
    {
        if (pPolygon->GetType() != pt)
            continue;

        for (auto pPoint = pPolygon->GetFirstPoint();
            pPoint != nullptr;
            pPoint = pPolygon->GetNextPoint())
        {
            pointCount++;
        }
    }
    timeSpan = GetTickCount() - startTime;
}

void DemoApp::ShowSimStats()
{
    struct {
        IPolygonPointProvider::PolygonType type;
        const char* name;
    }
    stats[2] =
    {
        {IPolygonPointProvider::SimulatedTrack, "Simulated"},
        {IPolygonPointProvider::BezierTrack, "Bezier"}
    };

    DWORD pointCount = 0;
    DWORD timeSpan = 0;
    std::ostringstream oss;

    for (int i = 0; i < 2; i++)
    {
        GetSimStats(timeSpan, pointCount, stats[i].type);

        if (i > 0)
            oss << "\n\n";

        oss << stats[i].name << ": " << pointCount << " points in ";

        if (timeSpan < 1)
            oss << "less than 1 ms";
        else
            oss << timeSpan << " ms. Performance: " <<
            float(pointCount) / float(timeSpan) << " points/ms";
    }

    MessageBoxA(m_hwnd, oss.str().c_str(), "Info", MB_OK);
}
