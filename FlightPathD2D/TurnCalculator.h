#pragma once

#include "GeomBase.h"

class TurnCalculator
{
private:
    Vect2f _position;  // current position
    Vect2f _dir1;      // direction -> normalized speed vector
    Vect2f _target;    // destination (routing) position
    float _radius;     // turn radius

    Vect2f _turnCenter; // turn origin (center)
    float _dirAngle;    // direction angle (heading)
    float _turnAngle;   // turn angle
    bool _isMissingTg;  // indicates target inside turn circle

    void CalculateTurn();

public:
    TurnCalculator() : _turnAngle(.0f), _radius(.0f) {}

    void Initialize(const Vect2f& position, float headingAngle, float turnRadius);
    void SetTarget(const Vect2f& targetPosition);
    void SetRadius(float turnRadius);

    float MoveBy(float distance);

    bool IsMissingTarget() { return _isMissingTg; }
    const Vect2f& GetPosition() { return _position; }
    const Vect2f& GetDirection() { return _dir1; }
};

inline void TurnCalculator::SetTarget(const Vect2f& targetPosition)
{
    _target = targetPosition;
    CalculateTurn();
}

inline void TurnCalculator::SetRadius(float turnRadius)
{
    _radius = turnRadius;
    CalculateTurn();
}
