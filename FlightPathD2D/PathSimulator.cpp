#include "PathSimulator.h"

PathSimulator::PathSimulator(const DocModel& model)
    : m_color(model._color)
    , m_route(model._path)
    , m_startHeading(model._heading)
    , m_startSpeed(model._speed)
    , m_startBank(model._bank)
    , m_simTimeStep(model._dt)
{
}

PathSimulator::~PathSimulator()
{
}

const D2D1_POINT_2F* PathSimulator::Reset()
{
    m_simTime = .0;
    m_distanceStep = m_startSpeed * m_simTimeStep;
    m_turnRadius = GetTurnRadius(m_startSpeed, m_startBank);
    
    if (m_route.size() > 1)
    {
        m_turnCalc.Initialize(m_route[0], m_startHeading, m_turnRadius);
        m_turnCalc.SetTarget(m_route[1]);
        m_routeIndex = 1;

        m_output = m_turnCalc.GetPosition();
        return &m_output;
    }
    else
    {
        m_routeIndex = 0;
        return nullptr;
    }
}

const D2D1_POINT_2F* PathSimulator::Simulate()
{
    if (!m_routeIndex)
        return nullptr;

    for(float dist = m_distanceStep;;)
    {
        dist = m_turnCalc.MoveBy(dist);

        if (!dist)
            break;
        
        if (++m_routeIndex >= m_route.size())
        {
            m_routeIndex = 0;
            break;
        }
            
        m_turnCalc.SetTarget(m_route[m_routeIndex]);
    }
    m_simTime += m_simTimeStep;
    m_output = m_turnCalc.GetPosition();
    return &m_output;
}

IPolygonPointProvider::PolygonType  PathSimulator::GetType()
{
    return SimulatedTrack;
}

UINT32 PathSimulator::GetColor()
{
    return m_color;
}

const D2D1_POINT_2F* PathSimulator::GetFirstPoint()
{
    return Reset();
}

const D2D1_POINT_2F* PathSimulator::GetNextPoint()
{
    return Simulate();
}
