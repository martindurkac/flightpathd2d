#include "SplinePath.h"

SplinePath::SplinePath(const DocModel& model)
    : m_color(model._color)
    , m_route(model._path)
    , m_startHeading(model._heading)
    , m_startSpeed(model._speed)
    , m_startBank(model._bank)
    , m_simTimeStep(model._dt)
    , m_segmentIdx(0)
{}

SplinePath::~SplinePath()
{}

IPolygonPointProvider::PolygonType SplinePath::GetType()
{
    return BezierTrack;
}

UINT32 SplinePath::GetColor()
{
    return 0x4040f0; // blue
}

const D2D1_POINT_2F* SplinePath::GetFirstPoint()
{
    m_segments.clear();

    if (m_route.size() > 1)
    {
        auto turnRadius = GetTurnRadius(m_startSpeed, m_startBank);

        m_segments.push_back({ m_route[0] });
        for (size_t i = 1; i < m_route.size(); ++i)
        {
            m_segments.push_back({ m_route[i] });
            m_segments.at(i).SetPrevious(&m_segments.at(i - 1));
        }

        for (auto& segment : m_segments)
            segment.SetDirection(turnRadius, &m_startHeading);

        m_segmentIdx = 1;
        m_segmentDistance = .0f;
        m_distanceStep = m_startSpeed * m_simTimeStep;
        m_pointBuffer = m_segments[m_segmentIdx].GetPoint(.0f);
        return &m_pointBuffer;
    }
    else
    {
        m_segmentIdx = 0;
        return nullptr;
    }
}

const D2D1_POINT_2F* SplinePath::GetNextPoint()
{
    if (!m_segmentIdx)
        return nullptr;

    m_segmentDistance += m_distanceStep;
    
    while (m_segmentIdx < m_segments.size())
    {
        auto& segment = m_segments.at(m_segmentIdx);
        float segmentLen = segment.GetLength();
        if (m_segmentDistance <= segmentLen)
        {
            m_pointBuffer = segment.GetPointByLength(m_segmentDistance);
            return &m_pointBuffer;
        }
        else
        {
            m_segmentDistance -= segment.GetLength();
            m_segmentIdx++;
        }
    }
    m_pointBuffer = m_segments.back().GetPoint(1.0f);
    m_segmentIdx = 0;

    return &m_pointBuffer;
}

SplinePath::Bezier3::Bezier3(const Vect2f& point)
    : _pos(point)
    , _dir(.0f, .0f)
    , _len(-1.f)
    , _hasPrev(false)
    , _hasNext(false)
{
}

void SplinePath::Bezier3::SetDirection(float turnRadius, float distance, const Vect2f& v, float vLen)
{
    // consider tunning the factor 0.5 for the distance 
    // to next (this) point or factor 2.0 for turnRadius
    float lenByRadius = 2.0f * turnRadius;
    float lenByDistance = 0.5f * distance;
    float resizeFactor = min(turnRadius, lenByDistance) / vLen;
    _dir = v * resizeFactor;
}

void SplinePath::Bezier3::SetDirection(float turnRadius, const float* heading/* = nullptr */)
{
    Vect2f toThis;
    Vect2f toNext;
    float toThisDist = GetVectToThis(toThis);
    float toNextDist = GetVectToNext(toNext);
    
    if (toThisDist > .0f)
    {
        if (toNextDist)
        {
            auto prevToNext = toThis + toNext;
            SetDirection(turnRadius, min(toThisDist, toNextDist), prevToNext, prevToNext.length());
        }
        else
            SetDirection(turnRadius, toThisDist, toThis, toThisDist);

        CalculateLength();
    }
    else if (toNextDist > .0f)
    {
        if (heading)
            SetDirection(turnRadius, toNextDist, { *heading, 1.f, Vect2f::Polar }, 1.f);
        else
            SetDirection(turnRadius, toNextDist, toNext, toNextDist);
    }
    else
    {
        _dir = { .0f, .0f };
    }
}

void SplinePath::Bezier3::CalculateLength()
{
    _len = .0f;
    _len2t.Reset();

    auto prev = Prev();
    if (prev)
    {
        std::map<float, Vect2f> points;
        points[0.f] = prev->_pos;
        points[1.f] = _pos;
        float initLen = (_pos - prev->_pos).length();
        
        // Epsilon value .1f looks sufficient to calculate
        // length of bezier segment. It evaluates cca 50 points
        _len = RefineLength(initLen, prev->_pos, _pos, .0f, 1.f, .1f, points);

        // setup linear interpolation of the function y=f(x) 
        // where y is t parameter of spline segment point(t), 
        // x is length if spline segment from initial point 
        // to point(t) 
        float currLen = .0f;
        Vect2f lastPoint = prev->_pos;
        for (auto it = points.begin(); it != points.end(); ++it)
        {
            currLen += (lastPoint - it->second).length();
            _len2t.AddSample(currLen, it->first);
            lastPoint = it->second;
        }
    }
}

float SplinePath::Bezier3::RefineLength(
    float len,
    const Vect2f& p0,
    const Vect2f& p1,
    float t0,
    float t1,
    float eps,
    std::map<float, Vect2f>& points)
{
    float tx = .5f * (t0 + t1);
    Vect2f px = GetPoint(tx);
    float len0x = (p0 - px).length();
    float len1x = (p1 - px).length();
    float len01 = len0x + len1x;
    points[tx] = px;

    if (fabsf(len - len01) < eps)
    {
        return len01;
    }
    else
    {
        return
            RefineLength(len0x, p0, px, t0, tx, eps, points) +
            RefineLength(len1x, px, p1, tx, t1, eps, points);
    }
}

Vect2f SplinePath::Bezier3::GetPoint(float t) const
{
    auto prev = Prev();
    if (!prev)
        return {};

    const Vect2f& p0 = prev->_pos;
    const Vect2f& p3 = _pos;

    if (t <= .0f)
        return p0;
    
    if (t >= 1.f)
        return p3;

    Vect2f p1 = p0 + prev->_dir;
    Vect2f p2 = p3 - _dir;
    
    float t2 = t * t;
    float t3 = t * t2;
    float tc = 1.f - t;
    float tc2 = tc * tc;
    float tc3 = tc * tc2;

    return
        p0 * tc3 +
        p1 * (3.f * tc2 * t) +
        p2 * (3.f * tc * t2) +
        p3 * t3;
}
