#include "TurnCalculator.h"
#include <sstream>

#define DEBUG_TRACE(value)  { std::ostringstream oss; \
    oss << "\n" << #value << ": " << value;           \
    OutputDebugStringA(oss.str().c_str()); }

void TurnCalculator::Initialize(const Vect2f& position, float headingAngle, float turnRadius)
{
    _position = position;
    _dir1 = Vect2f(headingAngle, 1.f, Vect2f::Polar);
    _dirAngle = headingAngle;
    _radius = turnRadius;
    _turnAngle = .0f;
}

void TurnCalculator::CalculateTurn()
{
    // default values
    _isMissingTg = false;
    _turnAngle = .0f;

    auto targetOnSide = _position.side(_target, _dir1);
    if (targetOnSide == Vect2f::Rear)
    {
        // target is directly on direction line behind of position
        _isMissingTg = true; 
    }
    else if (targetOnSide != Vect2f::Front)
    {
        Vect2f::DirectionType turnDir = 
            (targetOnSide == Vect2f::Left ? Vect2f::CCW : Vect2f::CW);

        _turnCenter = _position + _dir1.normal(turnDir, _radius);
        float posToTgAngle = _turnCenter.angle(turnDir, _position, _target);
        float tcToTgDistance = (_target - _turnCenter).length();

        if (tcToTgDistance <= _radius)
        {
            // target is inside of turning circle
            // at the end of turn the target is missed (passed by)
            _isMissingTg = true;
            _turnAngle = posToTgAngle;
        }
        else
        {
            // target is outside of turning circle
            // - calculate angle from end-of-turn position to target
            float eotToTgAngle = acosf(_radius / tcToTgDistance);

            if (turnDir == Vect2f::CW)
                eotToTgAngle = -eotToTgAngle;

            _turnAngle = posToTgAngle - eotToTgAngle;
        }
    }
}

float TurnCalculator::MoveBy(float distance)
{
    float remainingDistance = .0f;
    if (.0f == _turnAngle)
    {
        // move directly to target to avoid 
        // problems with float precision
        Vect2f pos2tg(_target - _position);
        float targetDistance = pos2tg.length();
        if (targetDistance >= distance)
        {
            _position += _dir1 * distance;
        }
        else
        {
            _position = _target;
            remainingDistance = distance - targetDistance;
        }
    }
    else
    {
        float moveAngle = .0f;
        float afterTurnDistance = distance - fabsf(_turnAngle) * _radius;
        if (afterTurnDistance > .0f)
        {
            moveAngle = _turnAngle;
            _turnAngle = .0f;
        }
        else
        {
            moveAngle = (_turnAngle > .0f ? distance : -distance) / _radius;
            _turnAngle -= moveAngle;
        }
        Vect2f delta(
            .5f * moveAngle + _dirAngle,
            2.f * _radius * fabsf(sinf(.5f * moveAngle)),
            Vect2f::Polar);

        _position += delta;
        _dirAngle += moveAngle;
        _dir1 = Vect2f(_dirAngle, 1.f, Vect2f::Polar);

        if (afterTurnDistance > .0f)
        {
            remainingDistance = 
                _isMissingTg ? afterTurnDistance : MoveBy(afterTurnDistance);
        }
    }
    return remainingDistance;
}
