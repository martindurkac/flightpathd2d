#define _SILENCE_TR1_NAMESPACE_DEPRECATION_WARNING

#include "gtest/gtest.h"

#include "../FlightPathD2D/LerpVector.h"
#include "../FlightPathD2D/GeomBase.h"

TEST(LerpVector, zeroSamples)
{
    LerpVector v;

    EXPECT_FLOAT_EQ(.0, v(0.0));
    EXPECT_FLOAT_EQ(.0, v(1.0));
    EXPECT_FLOAT_EQ(.0, v(2.0));
}

TEST(LerpVector, oneSample)
{
    LerpVector v;
    v.AddSample(1., 1.);

    EXPECT_FLOAT_EQ(1.0, v(0.0));
    EXPECT_FLOAT_EQ(1.0, v(1.0));
    EXPECT_FLOAT_EQ(1.0, v(2.0));
}

TEST(LerpVector, threeSamples)
{
    LerpVector v;
    v.AddSample(1., 1.);
    v.AddSample(2., 1.5);
    v.AddSample(3., 2.5);

    EXPECT_FLOAT_EQ(1.0,  v(0.0));
    EXPECT_FLOAT_EQ(2.5,  v(4.0));
    EXPECT_FLOAT_EQ(1.25, v(1.5));
    EXPECT_FLOAT_EQ(2.0,  v(2.5));
}

TEST(Vect2f, angle)
{
    Vect2f a{ 2.f, 1.f };
    Vect2f b{ 2.f, 2.f };
    Vect2f x{ 1.f, 1.f };

    EXPECT_FLOAT_EQ(x.angle(Vect2f::CCW, a, b), DEG_TO_RAD(45.f));
    EXPECT_FLOAT_EQ(x.angle(Vect2f::CW, b, a), DEG_TO_RAD(-45.f));
}
